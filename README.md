# Les capitaines investissent
_Et si on utilisait une partie de notre cash-out pour investir ensemble ?_

## Pourquoi faire ?

* Apprendre à lire des bilans
* Placer de l’argent
* Soutenir des jeunes pousses
* Se revoir et boire des coups

## Juridique : club d’investissement

C’est une structure jurdique très simple à mettre en œuvre et peu contraignante au fonctionnement (le niveau de paperasse est comparable à une association)
https://fr.wikipedia.org/wiki/Club_d%27investissement

Il faut au minum 5 membres, et au maximum 20.

## Quelle forme d’investissement

C’est très souple : prêt, capital, compte courant d’associé, etc.

## Auprès de qui on investit

C’est à nous de le décider. Ça peut être un prêt à Locomore, entrer dans le capital d’une start-up…
Probablement surtout du bouche à oreille pour savoir les besoins.

# Les sous

Il faut mettre entre 10 et 450€ chaque mois (ou un versement annuel).

#La durée

Le club dure maximum 10 ans. Les Cigales (club d’investissement dans l’économie sociale et solidaire) ont pour habitude de faire une phase de 5 ans d’investissement, et 5 ans de gestion pendant laquelle les entreprise financées ont le temps de racheter leurs actions.
Pour des raison pragmatiques, un lancement le 1er janvier 2017 semble judicieux.

# La gouvernance

Les règles sont à définir à la création. Cependant, il est habituel de :

* une personne == une voix, peu importe les sommes mises
* décision d’investissement à la majorité des voix exprimées
* droit de véto sur des formes particulières (par exemple achat de parts en coopératives qui ne peuvent pas générer de plus-value).